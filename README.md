# prometheus-grafana

Prometheus and Grafana configuration using docker-compose

## Prometheus setup with docker-compose
- Remember to move the file `prometheus.yml` inside the folder `prometheus/prometheus`
- Start up Prometheus deamon with docker-compose using command:
```
docker-compose up -d
```
- Nagivate to the address `http://localhost:9000` in the browser to open Prometheus

- To terminate the Prometheus and Grafana containers:
```
docker-compose down
```
The address for Grafana is `http://localhost:3000` and Prometheus is `http://localhost:9090`

In order to add Prometheus datasource in Grafana, you need to add this address `http://127.0.0.1:9090` in the data source config of Grafana